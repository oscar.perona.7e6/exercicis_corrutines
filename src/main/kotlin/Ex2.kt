import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.util.*

fun main() {
    val scanner= Scanner(System.`in`)

    runBlocking {
        println("Cuantes vegades vols escriure")
        val num = scanner.nextInt()
        repetition(num)
    }
    println("Finished")
}
suspend fun repetition(num :Int) {
    coroutineScope {
        for (i in 1..num) {
            launch {
                println("HelloWord  n$i")
            }
            delay(100)
        }
    }
}
