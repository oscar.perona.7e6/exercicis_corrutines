import kotlinx.coroutines.*

fun main() {
    runBlocking {
        coroutineScope {
            launch { corroutine1() }
            launch { corroutine2() }
        }
    }
    println("Completed")
}
private suspend fun corroutine1() {

    println("Hello World 1.1")
    delay(3000)
    println("Hello World 1.2")
    delay(3000)
    println("Hello World 1.3")
}

private suspend fun corroutine2() {

    println("Hello World 2.1")
    delay(3000)
    println("Hello World 2.2")
    delay(3000)
    println("Hello World 2.3")
}

