import kotlinx.coroutines.*
import java.io.File
import kotlin.system.measureTimeMillis

fun main() {
    val song = File("./src/main/data/song.txt").readLines()
    var time : Long
    runBlocking {
        time = measureTimeMillis {
            for (line in song) {
                withContext(Dispatchers.IO) {
                    launch {
                        println(line)
                        delay(3000)
                    }
                }
            }
        }
        println("${time/1000} seconds")
    }

}

