import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main() {
    println("The main program is started")

    GlobalScope.launch {
        doBackground()
    }
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

private suspend fun doBackground() {
    println("Background processing started")
    delay(1000)
    println("Background processing finished")
}


/*
The main program is started
The main program continues
Background processing started
The main program is finished
 */