import kotlinx.coroutines.*

suspend fun main() {
    println("The main program is started")
    doBackground()
    println("The main program continues")
    runBlocking {
        delay(1500)
        println("The main program is finished")
    }
}

private suspend fun doBackground() {
    withContext(Dispatchers.IO) {
        println("Background processing started")
        delay(1000)
        println("Background processing finished")
    }
}
