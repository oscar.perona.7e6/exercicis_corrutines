import kotlinx.coroutines.*
import kotlin.system.exitProcess

var haveWinner = false

fun main(){

    runBlocking {
        coroutineScope {
            launch { printHorseStatus(1) }
            launch { printHorseStatus(2) }
            launch { printHorseStatus(3) }
            launch { printHorseStatus(4) }
        }
    }
}

private suspend fun printHorseStatus(horseNumber:Int){

    for (i in 1..4){
        val range = 1..10000

        delay(range.random().toLong())
        println("El caballo numero $horseNumber va por la seccion $i/4")
    }

    if (!haveWinner){
        haveWinner = true

        println()
        println("EL CABALLO GANADOR HA SIDO EL CABALLO NUMERO $horseNumber")

        exitProcess(0)
    }
}