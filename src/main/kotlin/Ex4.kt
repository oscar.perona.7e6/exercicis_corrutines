import kotlinx.coroutines.*
import java.util.*
import kotlin.system.exitProcess

val scanner= Scanner(System.`in`)

fun main() {
    println("Adivina un numero ente 1 y 50 en menos de 10 segundos")
    runBlocking {
        GlobalScope.launch{
            guessNumber()
        }
        delay(10000)
        println("\nSe acabo el tiempo, has perdido")
    }
}

fun guessNumber(){
    val randomNumber = (1..50).random()
    do{
        val userNum = scanner.nextInt()

        if (userNum != randomNumber){
            println("Incorrecto\n")
        }

    }while (userNum != randomNumber)
    println("Has acertado!")
    exitProcess(0)
}
